"use strict";

// ------Our Services Tabs------

const tabs = document.querySelector(".tabs");
const tabsContent = document.querySelector(".tabs-content").children;

tabs.addEventListener("click", function (event) {
    tabs.querySelector(".active-service").classList.remove("active-service")
    event.target.classList.add("active-service")
    for (let element of tabsContent) {
        if (event.target.id === element.dataset.filter) {
            element.classList.add("active-content");
        } else {
            element.classList.remove("active-content")
        }
    }
})

// -----------Our Work----------

const workTabs = document.querySelector(".work-tabs");
const workExample = document.querySelector(".work-examples").children;
const workTabsItems = document.querySelectorAll(".work-example-card");
const loadMore = document.querySelector(".our-work-button");

workTabs.addEventListener("click", function (event) {
    workTabs.querySelector(".active-work-tab").classList.remove("active-work-tab");
    event.target.classList.add("active-work-tab");
    for (let element of workExample) {
        let loadedIten = element.getAttribute("data-loaded");
        if (event.target.id === "All" && loadedIten === true) {
            element.style.display = "block";
        } else if (element.className.includes("hidden")) {
            element.style.display = "none";
        } else if (event.target.id === "All") {
            element.style.display = "block";
        } else if (event.target.id !== element.dataset.set) {
            element.style.display = "none";
        } else if (event.target.id === element.dataset.set) {
            element.style.display = "block";
        }
    }
})

function setCategory() {
    for (let i = 0; i < workTabsItems.length; i++) {
        let category = workTabsItems[i].querySelector(".work-card-category");
        category.textContent = workTabsItems[i].dataset.set;;
    }
}
setCategory()

loadMore.addEventListener("click", function () {
    for (let element of workExample) {
        element.setAttribute("data-loaded", "loaded")
        element.classList.remove("hidden")
        element.style.display = "block";
    }
    workTabs.querySelector(".active-work-tab").classList.remove("active-work-tab");
    workTabs.querySelector("#All").classList.add("active-work-tab");
    loadMore.remove();
})

// ------What People Say About theHam------

let currentSliderPhoto = 0;
const activeAutorSlider = document.querySelectorAll(".slider-photo");
const review = document.querySelectorAll(".review-item");
const nextArrow = document.querySelector(".right-arrow");
const previousArrow = document.querySelector(".left-arrow");

for (let i = 0; i < activeAutorSlider.length; i++) {
    activeAutorSlider[i].addEventListener("click", function () {
        currentSliderPhoto = i;
        document.querySelector('.review-item.active-review').classList.remove("active-review");
        document.querySelector('.slider-photo.active-slider-photo').classList.remove("active-slider-photo");
        activeAutorSlider[currentSliderPhoto].classList.add("active-slider-photo");
        review[currentSliderPhoto].classList.add("active-review");
    })
}

function slide(e) {
    currentSliderPhoto = (e + review.length) % review.length;
}

nextArrow.addEventListener("click", function () {
    review[currentSliderPhoto].classList.remove('active-review');
    activeAutorSlider[currentSliderPhoto].classList.remove("active-slider-photo");
    slide(currentSliderPhoto + 1);
    review[currentSliderPhoto].classList.add("active-review");
    activeAutorSlider[currentSliderPhoto].classList.add("active-slider-photo");
});

previousArrow.addEventListener("click", function () {
    review[currentSliderPhoto].classList.remove('active-review');
    activeAutorSlider[currentSliderPhoto].classList.remove("active-slider-photo");
    slide(currentSliderPhoto - 1);
    review[currentSliderPhoto].classList.add("active-review");
    activeAutorSlider[currentSliderPhoto].classList.add("active-slider-photo");
});
